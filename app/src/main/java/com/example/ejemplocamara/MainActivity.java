package com.example.ejemplocamara;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private ImageView imageView;

    ActivityResultLauncher<Intent> resultAct = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    // Comprobamos que el código de respuesta es correcto
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // Obtenemos el Intent devuelto
                        Intent datosResult = result.getData();

                        Bundle extras = datosResult.getExtras();

                        // Obtnemos los datos devueltos por el Intent y lo guardamos en un objeto de tipo Bitmap
                        Bitmap imageBitmap = (Bitmap) extras.get("data");

                        // "Pegamos" ese bitmap en el ImageView
                        imageView.setImageBitmap(imageBitmap);
                    }
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button botonFoto = findViewById(R.id.botonHacerFoto);
        imageView = findViewById(R.id.imageView);

        botonFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Lanzamos un Intent implícito para usar la aplicación de la cámara del dispositivo
                Intent hacerFotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                if(hacerFotoIntent.resolveActivity(getPackageManager())!=null)
                    resultAct.launch(hacerFotoIntent);


            }
        });
    }
}